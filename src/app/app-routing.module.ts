import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsLayoutComponent } from './modules/products/components/products-layout/products-layout.component';
import { ProductDetailsComponent } from './modules/products/components/product-details/product-details.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductsLayoutComponent,
    children: [
      {
        path: ':id',
        component: ProductDetailsComponent
      }
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'products'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
