import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SigninComponent } from 'src/app/modules/auth/components/signin/signin.component';
import { SignupComponent } from 'src/app/modules/auth/components/signup/signup.component';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isAuthenticated$: Observable<{isAuthenticated: boolean}>;

  constructor(private bsModalService: BsModalService, private authService: AuthService) { }

  ngOnInit() {
    this.isAuthenticated$ = this.authService.isAuthenticated$;
  }

  onSignIn() {
    this.bsModalService.show(SigninComponent);
  }

  onSignUp() {
    this.bsModalService.show(SignupComponent);
  }

  onSignOut() {
    this.authService.signout();
  }
}
