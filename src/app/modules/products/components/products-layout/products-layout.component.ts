import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../products.service';
import { IProduct } from 'src/app/shared/models/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-layout',
  templateUrl: './products-layout.component.html',
  styleUrls: ['./products-layout.component.scss']
})
export class ProductsLayoutComponent implements OnInit {
  products: IProduct[];
  isLoading = true;

  constructor(private productsService: ProductsService, private router: Router) { }

  ngOnInit() {
    this.productsService.initProductsList();
    this.productsService.products$
      .subscribe( (products: IProduct[]) => {
        if (products) {
          this.isLoading = false;
        } else {
          this.isLoading = true;
        }
        this.products = products;
      });
  }
}
