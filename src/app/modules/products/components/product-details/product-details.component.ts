import { Component, OnInit, OnDestroy } from '@angular/core';
import { IProduct, IProductReview } from 'src/app/shared/models/models';
import { ProductsService } from '../../products.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  productReviews: IProductReview[] = [];
  product: IProduct;
  selectedProductSubscr: Subscription;
  activatedRouteSubscr: Subscription;
  environment = environment;

  rate: number;
  productDetailsLoading = true;

  constructor(private productsService: ProductsService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.selectedProductSubscr = this.productsService.selectedProduct$
      .subscribe((product: IProduct) => {
        this.productDetailsLoading = false;
        this.product = product;
      });

      this.activatedRouteSubscr = this.activatedRoute
        .params
        .subscribe( (params) => {
          this.productDetailsLoading = true;
          const productId = +params.id;
          this.productsService.getProductById(productId);
        });
  }

  ngOnDestroy() {
    this.selectedProductSubscr.unsubscribe();
    this.activatedRouteSubscr.unsubscribe();
  }
}
