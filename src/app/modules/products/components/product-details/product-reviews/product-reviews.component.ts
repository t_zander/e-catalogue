import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { IProductReview } from 'src/app/shared/models/models';
import { ProductsService } from '../../../products.service';
import { Subscription, Observable } from 'rxjs';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { AuthService } from 'src/app/modules/auth/services/auth.service';

@Component({
  selector: 'app-product-reviews',
  templateUrl: './product-reviews.component.html',
  styleUrls: ['./product-reviews.component.scss']
})
export class ProductReviewsComponent implements OnInit, OnDestroy {
  reviewsLoading = true;
  productId: number | string;
  productReviews: IProductReview[] = [];
  activatedRouteSubscr: Subscription;
  isAuthenticated$: Observable<{isAuthenticated: boolean}>;

  constructor(
    private productsService: ProductsService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.isAuthenticated$ = this.authService.isAuthenticated$;

    this.activatedRouteSubscr = this.activatedRoute
      .params
      .pipe(
        switchMap(params => {
          this.productId = params.id;
          // чтоб не показывать неактуальные данные пока не загрузились новые
          this.reviewsLoading = true;
          return this.productsService.getCurrentProductReviews(params.id);
        })
      )
      .subscribe(
        (productReviews: IProductReview[]) => {
          this.reviewsLoading = false;
          this.productReviews = productReviews;
        },
        _ => {
          this.notificationsService.notify('Cannot fetch product reviews. Please try again later');
          this.productReviews = [];
        }
      );
  }

  onAddReview(review: IProductReview) {
    this.productsService.addReview(review, this.productId)
      .subscribe(
        (response: {review_id: number})  => {
          const productReviewsCopy = [...this.productReviews];
          const newReview = {...review, ...{id: response.review_id} };
          productReviewsCopy.push(newReview);
          this.productReviews = productReviewsCopy;
        },
        _ => {
          this.notificationsService.notify('Cannot add a review. Please try again later');
        }
      );
  }

  ngOnDestroy() {
    this.activatedRouteSubscr.unsubscribe();
  }
}
