import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-add-product-review',
  templateUrl: './add-product-review.component.html',
  styleUrls: ['./add-product-review.component.scss']
})
export class AddProductReviewComponent implements OnInit {
  @ViewChild('reviewForm') reviewForm;
  @Output() reviewAdded = new EventEmitter();

  reviewText = '';
  rate = 0;
  isRateInvalid;

  constructor() { }

  ngOnInit() {
  }

  onPostReview() {
    if (this.rate === 0) {
      this.isRateInvalid = true;
    } else {
      this.isRateInvalid = false;
      const review = {
        rate: this.rate,
        text: this.reviewText
      };
      this.reviewForm.resetForm();
      this.reviewAdded.emit(review);
    }
  }
}
