import { Component, OnInit, Input } from '@angular/core';
import { IProductReview } from 'src/app/shared/models/models';

@Component({
  selector: 'app-product-review',
  templateUrl: './product-review.component.html',
  styleUrls: ['./product-review.component.scss']
})
export class ProductReviewComponent implements OnInit {
  @Input() productReview: IProductReview;
  rate: number;
  constructor() { }

  ngOnInit() {}
}
