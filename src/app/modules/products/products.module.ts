import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProductsLayoutComponent } from './components/products-layout/products-layout.component';
import { ProductComponent } from './components/products-layout/product/product.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductReviewsComponent } from './components/product-details/product-reviews/product-reviews.component';
import { ProductReviewComponent } from './components/product-details/product-reviews/product-review/product-review.component';

import { AlertModule } from 'ngx-bootstrap/alert';
import { RatingModule } from 'ngx-bootstrap/rating';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { AddProductReviewComponent } from './components/product-details/product-reviews/add-product-review/add-product-review.component';

@NgModule({
  declarations: [
    ProductsLayoutComponent,
    ProductComponent,
    ProductDetailsComponent,
    ProductReviewComponent,
    ProductReviewsComponent,
    AddProductReviewComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RatingModule,
    AlertModule
  ]
})
export class ProductsModule { }
