import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { IProduct, IProductReview } from 'src/app/shared/models/models';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private _products = new BehaviorSubject<IProduct[]>(null);
  private _selectedProduct = new Subject<IProduct>();

  public products$ = this._products.asObservable();
  public selectedProduct$ = this._selectedProduct.asObservable();

  constructor(private http: HttpClient, private notificationsService: NotificationsService) { }

  initProductsList(): void {
    this.http.get<IProduct[]>(`${environment.apiUrl}products`)
      .subscribe(
        (products: IProduct[]) => {
          this._products.next(products);
        },
        _ => {
          this.notificationsService.notify('Cannot fetch products list. Please try again later');
          this._products.next([]);
        }
      );
  }

  getProductById(productId) {
    this.http.get<IProduct[]>(`${environment.apiUrl}products`)
      .pipe(
        map( (products: IProduct[]) => {
          return products.find(product => product.id === productId);
        })
      )
      .subscribe(
        (product: IProduct) => {
          this._selectedProduct.next(product);
        },
        _ => {
          this.notificationsService.notify('Cannot fetch product. Please try again later');
          this._products.next([]);
        }
      );
  }

  addReview(review: IProductReview, productId: number | string) {
    return this.http.post(`${environment.apiUrl}reviews/${productId}`, review);
  }

  getCurrentProductReviews(productId) {
    return this.http.get<IProductReview[]>(`${environment.apiUrl}reviews/${productId}`);
  }
}
