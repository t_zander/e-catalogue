import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IAuthResponse } from 'src/app/shared/models/models';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  signInForm: FormGroup;
  feedBack: {type: string, message: string};
  get username() { return this.signInForm.get('username'); }
  get password() { return this.signInForm.get('password'); }

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private bsModalRef: BsModalRef) { }

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    this.signInForm = this.fb.group({
      'username': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required]]
    });
  }

  onSignIn() {
    this.authService.signin(this.signInForm.value)
      .subscribe(
        (response: IAuthResponse) => {
          if (response.success) {
            this.bsModalRef.hide();
          } else {
            this.feedBack = {type: 'error', message: response.message};
          }
        },
        error => {
          console.log(error);
        }
      );
  }
}
