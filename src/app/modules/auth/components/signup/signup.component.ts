import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { IAuthResponse } from 'src/app/shared/models/models';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;
  feedBack: {result: string, message: string};

  get username() { return this.signUpForm.get('username'); }
  get password() { return this.signUpForm.get('password'); }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private bsModalRef: BsModalRef) { }

  ngOnInit() {
    this._initForm();
  }

  private _initForm() {
    this.signUpForm = this.fb.group({
      'username': ['', [Validators.required, Validators.email]],
      'password': ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  onNavigateToMain() {
    this.bsModalRef.hide();
    this.router.navigate(['/products']);
  }

  onSignUp() {
    this.authService.signup(this.signUpForm.value)
      .subscribe(
        (response: IAuthResponse) => {
          if (response.success) {
            this.authService.setCurrentUser(response);
            this.feedBack = {result: 'success', message: 'Successfully signed up!'};
          } else {
            this.feedBack = {result: 'error', message: 'User with such email already exists!'};
          }
        },
        _ => {
          this.feedBack = {result: 'error', message: 'Cannot sign up. try again later'};
        }
      );
  }
}
