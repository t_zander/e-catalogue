import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { AlertModule } from 'ngx-bootstrap/alert';

@NgModule({
  declarations: [SigninComponent, SignupComponent],
  imports: [
    CommonModule,
    SharedModule,
    AlertModule.forRoot()
  ],
  entryComponents: [
    SigninComponent,
    SignupComponent
  ]
})
export class AuthModule { }
