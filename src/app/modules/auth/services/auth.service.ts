import { Injectable } from '@angular/core';
import { IUser, IAuthResponse } from 'src/app/shared/models/models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isAuthenticated = new BehaviorSubject({isAuthenticated: false});
  public readonly isAuthenticated$ = this._isAuthenticated.asObservable();

  constructor(private http: HttpClient) { }

  getToken(): string {
    if (localStorage.getItem('token')) {
      return localStorage.getItem('token');
    } else {
      return null;
    }
  }

  private _setToken(token: string) {
    localStorage.setItem('token', token);
  }

  private _unsetToken() {
    if (this.getToken) {
      localStorage.removeItem('token');
    }
  }

  setCurrentUser(response: IAuthResponse) {
    console.log(response);
    this._setToken(response.token);
    this._isAuthenticated.next({isAuthenticated: response.success});
  }

  checkAuth() {
    if (this.getToken()) {
      this._isAuthenticated.next({isAuthenticated: true});
    }
  }

  signin(userInfo: IUser) {
    return this.http.post(`${environment.apiUrl}login/`, userInfo)
      .pipe(
        tap( (response: IAuthResponse) => {
          if (response.success) {
            this.setCurrentUser(response);
          }
        })
      );
  }

  signup(userInfo: IUser) {
    return this.http.post(`${environment.apiUrl}register/`, userInfo);
  }

  signout() {
    this._unsetToken();
    this._isAuthenticated.next({isAuthenticated: false});
  }
}
