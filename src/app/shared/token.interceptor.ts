import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { AuthService } from '../modules/auth/services/auth.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationsService } from './services/notifications.service';

@Injectable({
  providedIn: 'root'
})

export class TokenInterceptor implements HttpInterceptor {
  constructor(public authService: AuthService, private notificataionsService: NotificationsService) {}
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    

    if (this.authService.getToken()) {
      request = request.clone({
        setHeaders: {
          Authorization: `Token ${this.authService.getToken()}`
        }
      });
    }
    return next.handle(request)
      .pipe(
        tap(
          (response: HttpResponse<any>) => {
            if (response.status === 401) {
              this.notificataionsService.notify('Session time finished. Please login again');
              this.authService.signout();
            }
          }
        )
      );
  }

}
