export interface IProduct {
  id: number;
  img: string;
  text: string;
  title: string;
}

export interface IProductReview {
  id?: number;
  product?: number;
  created_by?: {
    id: number,
    username: string
  };
  rate: number;
  text: string;
}

export interface IUser {
  id?: number;
  username: string;
  password: string;
}

export interface IAuthResponse {
  success: boolean;
  token?: string;
  message?: string;
}
