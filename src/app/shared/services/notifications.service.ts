import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ErrorComponent } from '../error/error.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  bsModalRef: BsModalRef;
  
  constructor(private modalService: BsModalService) { }

  notify(message) {
    const initialState = {
      message
    };
    this.bsModalRef = this.modalService.show(ErrorComponent, {initialState});
  }
}
